from django.db.models import F, Count, Max
from django.db import models

# Here will be my answers:
# ----------------------------
# Q1 Answer:
# qs = Country.objects.annotate(num_cities=Count('cities'))
# qs[0].num_cities
# ----------------------------
# Q2 Answer:
# qs = Country.objects.annotate(biggest_city_size=Max('cities__area'))
# qs[0].biggest_city_size
# ----------------------------
# Q3 Answer:
# qs = City.objects.annotate(density=F('population') / F('area'))
# qs[0].density


class CountryQuerySet(models.QuerySet):
    def with_num_cities(self):
        return self.annotate(num_cities=Count('cities'))

    def with_biggest_city_size(self):
        return self.annotate(Max('cities__area'))


class CityQuerySet(models.QuerySet):
    def with_density(self):
        return self.annotate(density=F('population') / F('area'))


class Country(models.Model):
    objects = CountryQuerySet.as_manager()
    name = models.CharField('Название', max_length=255)


class City(models.Model):
    objects = CityQuerySet.as_manager()
    country = models.ForeignKey(Country, verbose_name='Страна', related_name='cities', on_delete=models.CASCADE)
    name = models.CharField('Название', max_length=255)
    population = models.FloatField('Население')
    area = models.FloatField('Площадь')
